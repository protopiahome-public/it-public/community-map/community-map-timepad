<?php

require_once("config.php");
require_once("graphql_functions.php");

if ($_REQUEST["secret"] != $telegram_secret)
{
	//die();
}

$data = json_decode(file_get_contents('php://input'), true);
file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);
file_put_contents(__DIR__ . "/log.txt", print_r($data, true), FILE_APPEND);
//die();

protopia_auth();

$fields = ["input: EventInput" => [
	"title" => $data["name"],
	"description" => $data["description_short"],
	"external_id" => $data["id"],
	"external_system" => "timepad",
	"external_url" => $data["url"],
	"start_date" => date("c", strtotime($data["starts_at"]["date"])),
	"end_date" => date("c", strtotime($data["ends_at"]["date"])),
//	"owner" => $data["organization"]["name"],
//	"place" => $data["location"]["city"],
	"latitude" => $data["location"]["coordinates"][0],
	"longitude" => $data["location"]["coordinates"][1],
]];

if ($result = protopia_query("getEventByExternal", "_id", ["external_id:String" => $data["id"], "external_system:String" => "timepad"]))
{
	$fields["id:ID"] = $result["_id"];
}

$result = protopia_mutation("changeEvent", "_id", $fields);

file_put_contents(__DIR__ . "/log.txt", print_r($result, true), FILE_APPEND);